import React from "react";
import { useNavigate } from 'react-router-dom'
import { Row, Col, Button, Table } from "reactstrap";

function CartSummary(props) {
    const navigate = useNavigate();

    // Sub Total of All Added Products
    const subTotal = props.cartItems.reduce((total, item) => total = total + item.qty * item.price, 0)?.toFixed(2);
    // Add 1.23% Tax in Each Products
    const tax = props.cartItems.reduce((total, item) => total = total + (((item.qty * item.price) * 1.23) / 100), 0)?.toFixed(2);
    // Product Shipping Charge
    const shipping = 500.00;

    function onPayNow() {
        localStorage.removeItem('cartItems');
        navigate('/success');
    }

    return (
        <div className="mt-5">
            <Row>
                <Col md={8}></Col>
                <Col md={4}>
                    <h2>Cart Summary</h2>
                    <hr />
                    <Table borderless className='text-left'>
                        <tbody>
                            <tr>
                                <td>
                                    <h5>Sub Total:</h5>
                                </td>
                                <td>
                                    <h5>₹{subTotal}</h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>Tax (1.23% per product):</h5>
                                    {props.cartItems?.map(item => {
                                        return (
                                            <p className="px-2" key={item.id}>1.23% of ₹{(item.qty * item.price)?.toFixed(2)}</p>
                                        )
                                    })}
                                </td>
                                <td>
                                    <h5>₹{tax}</h5>
                                    {props.cartItems?.map(item => {
                                        return (
                                            <p className="px-2" key={item.id}>₹{(((item.qty * item.price) * 1.23) / 100)?.toFixed(2)}</p>
                                        )
                                    })}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>Shipping (500 LKR):</h5>
                                </td>
                                <td>
                                    <h5>₹500.00</h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h3>Grand Total:</h3>
                                </td>
                                <td>
                                    {/* Sum of All Shipping + Tax + Sub Total */}
                                    <h3>₹{(parseFloat(subTotal) + parseFloat(tax) + parseFloat(shipping))?.toFixed(2)}</h3>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                    {/* Pay Now Button */}
                    <Button color="success" size="lg" className="mt-5 mb-5" onClick={() => onPayNow()}>
                        Pay Now
                    </Button>
                </Col>
            </Row>
        </div>
    )

}
export default CartSummary;