import React from "react";
import { useNavigate } from 'react-router-dom'
import { Row, Col, Button } from "reactstrap";
import Logo from '../assets/images/dummy-logo.png'

function Header() {
    const navigate = useNavigate();

    return (
        <Row>
            {/* Company Logo */}
            <Col md={2}>
                <img src={Logo} height={70} width={150} className='mt-3 cursor-pointer' alt='logo' onClick={() => navigate('/')} />
            </Col>
            <Col md={8}></Col>
            <Col md={2}>
                {/* Cart Button */}
                <Button color="primary" className="mt-3" onClick={() => navigate('/cart')}>
                    Cart
                </Button>
            </Col>
        </Row>
    )

}
export default Header;