import React, { Fragment, useEffect, useState } from "react";
import { Table, Button } from "reactstrap"
import CartSummary from "./CartSummary";

function Cart() {

    const [cartItems, setCartItems] = useState([]);

    // List all Products Which Added in Cart
    useEffect(() => {
        setCartItems(localStorage.getItem('cartItems') ? JSON.parse(localStorage.getItem('cartItems')) : []);
    }, []);

    // Remove All Products From Cart
    const onRemoveAll = () => {
        localStorage.removeItem('cartItems');
        setCartItems([]);
    }

    // When Qty Changed Update Qty in Cart Data 
    const onQtyChange = (value, index) => {
        let temp = [...cartItems];
        temp[index].qty = value > 0 ? value : 1;
        localStorage.setItem('cartItems', JSON.stringify(temp));
        setCartItems(temp);
    }

    // Remove Single Products From Cart
    const onRemoveItem = (item) => {
        let temp = cartItems.filter(v => v.id !== item.id);
        localStorage.setItem('cartItems', JSON.stringify(temp));
        setCartItems(temp);
    }

    return (
        <Fragment>
            {/* Cart is Not Empty */}
            {cartItems?.length > 0 ?
                <Fragment>
                    <h2 className="mt-3">Cart Items</h2>
                    <div className="w-100 text-right">
                        <Button color="danger" size="lg" className="m-3" onClick={onRemoveAll}>Remove All</Button>
                    </div>
                    <Table bordered={true}>
                        <thead>
                            <tr>
                                <th>Product Image</th>
                                <th>Product Name</th>
                                <th>Product Qty</th>
                                <th>Product Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {cartItems?.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>
                                            <img src={item.image} height={50} width={50} alt={item.title} />
                                        </td>
                                        <td>
                                            {item.title}
                                        </td>
                                        <td>
                                            <Button size="sm" color="dark" onClick={(e) => onQtyChange((parseInt(item.qty) - 1), index)}>-</Button>
                                            <input value={item.qty} className='qty_change' onChange={(e) => onQtyChange(e.target.value, index)} />
                                            <Button size="sm" color="dark" onClick={(e) => onQtyChange((parseInt(item.qty) + 1), index)}>+</Button>
                                        </td>
                                        <td>
                                            ₹{item.price} * {item.qty} = <b>₹{(item.qty * item.price).toFixed(2)}</b>
                                        </td>
                                        <td>
                                            <Button color="danger" onClick={() => onRemoveItem(item)}>Remove</Button>
                                        </td>
                                    </tr>
                                )
                            })
                            }
                            <tr>
                                <td colSpan={3}>
                                   <h5>Sub Total: </h5> 
                                </td>
                                <td>
                                    <h5><b>₹{cartItems.reduce((total, item) => total = total + item.qty * item.price,0)?.toFixed(2)}</b></h5>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </Table>
                    <CartSummary cartItems={cartItems} />
                </Fragment> :
                // Cart is Empty
                <h3 className="mt-5">Cart is Empty!</h3>
            }

        </Fragment>
    )

}
export default Cart;