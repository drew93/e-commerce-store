import React, { Fragment, useState } from "react";
import { Row, Col, Button } from "reactstrap";
import products from '../products.json';
import Card from 'react-bootstrap/Card';
import Notification from "../Container/Notification";
import { useEffect } from "react";


function ProductList() {

    const [showAlert, setShowAlert] = useState({ open: false, message: '', severity: '' });
    const [allProducts, setAllProducts] = useState([]);
    const [cartItems, setCartItems] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);
    // Fetch item added with quntity
    function fetchData() {
        let items = localStorage.getItem('cartItems') ? JSON.parse(localStorage.getItem('cartItems')) : [];
        products.map(item => {
            var findIndx = items.findIndex(v => v.id === item.id);
            item.qty = findIndx === -1 ? 0 : items[findIndx].qty;
            return true;
        })
        setAllProducts(products);
        setCartItems(items);
    }

    // Product Add in Cart
    const onAddCart = (item) => {
        // get Cart Products From Localstorage
        let cartData = [...cartItems];
        // Find If Product Already Added
        var findIndx = cartData.findIndex(v => v.id === item.id);
        // Add Qty Of Product
        item.qty = item.qty === 0 ? 1 : item.qty;
        // Add New Product In Cart If It Is Not There
        findIndx === -1 ? cartData.push(item) : cartData[findIndx] = item;
        // Set Data In Localstorage
        localStorage.setItem('cartItems', JSON.stringify(cartData));
        setCartItems(cartData);
        // For Show Alert
        setShowAlert({
            open: true,
            message: 'Product added to cart',
            severity: 'success',
        });
    }

    // When Qty Changed Update Qty in Cart Data 
    const onQtyChange = (value, index, item) => {
        // Change Qty in All Procducts
        let temp = [...allProducts];
        temp[index].qty = value > 0 ? value : 0;
        setAllProducts(temp);
        // If Product is added To Cart then change Qty in Cart
        var findIndx = cartItems.findIndex(v => v.id === item.id);
        if (findIndx !== -1) {
            let items = [...cartItems];
            items[findIndx].qty = value;
            localStorage.setItem('cartItems', JSON.stringify(items));
            setCartItems(items);
        }

    }

    return (
        <Fragment>
            {/* Notification For Product Add to Cart */}
            {showAlert.open && <Notification {...showAlert} handleClose={() => setShowAlert({ open: false, message: '', severity: '' })} />}
            <h2 className="mb-5">E-Commerce Stores</h2>
            <Row className="main_productlist">
                {/* All Products List */}
                {allProducts?.map((item, index) => {
                    var productIndex = cartItems.findIndex(v => v.id === item.id);
                    return (
                        <Col md={3} className='product' key={index}>
                            <Card style={{ width: '200', height: '100%' }}>
                                <img src={item.image} height={350} width='100%' alt={item.title} />
                                <Card.Body>
                                    <Row>
                                        <Col md={8} className='text-left text-center'>
                                            <Card.Title>{item.title} </Card.Title>
                                        </Col>
                                        <Col md={4}>
                                            <h3>₹{item.price}</h3>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={6} className='text-left text-center'>
                                            <Button size="sm" color="dark" onClick={(e) => onQtyChange((parseInt(item.qty) - 1), index, item)}>-</Button>
                                            <input value={item.qty} className='qty_change' onChange={(e) => onQtyChange(e.target.value, index, item)} />
                                            <Button size="sm" color="dark" onClick={(e) => onQtyChange((parseInt(item.qty) + 1), index, item)}>+</Button>
                                        </Col>
                                        <Col md={6}>
                                            {/* If Product Already Added To Cart Then Disable Button */}
                                            <Button className='add-cart-btn' color="primary" outline disabled={productIndex !== -1} onClick={() => onAddCart(item)}>{productIndex === -1 ? 'Add To Cart' : 'Added To Cart'}</Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
        </Fragment>
    )

}
export default ProductList;