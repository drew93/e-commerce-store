import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import './assets/css/common.css';
import './assets/css/style.css';
import ProductList from './Components/ProductList';
import Header from './Components/Header';
import { Route,  BrowserRouter as Router, Routes } from 'react-router-dom';
import Cart from './Components/Cart';
import Success from './Components/Success';

function App() {
  return (
    <div className="App">
      <Router>
        {/* Header */}
        <Header />
        <Routes>
          {/* Define All Pages Pathname */}
          <Route path="/" element={<ProductList />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/success" element={<Success />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
